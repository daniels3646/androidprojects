package team2.shattlebip;

public class Message {
    public String msg;
    public Message (String message)
    {
       this.msg = message;
    }
    public Game.Stage getGameStage()
    {
        if(this.msg.charAt(0)=='0')
        {
            return Game.Stage.ARRANGING;
        }
        else {
            if(this.msg.charAt(0)=='1') {
                return Game.Stage.BATTLING;
            }
            else{
                return null;
            }
        }
    }
    public Boolean isMyTurn()
    {
        return ((this.msg.charAt(1)=='1' && MainMenu.ServOrClient=="server") || (this.msg.charAt(1)=='0' && MainMenu.ServOrClient=="client"));

    }

    public boolean didHeUpgrade(){
        if(this.msg.length()>2) {
            return (this.msg.charAt(2) == '1');
        }else{
            return false;
        }
    }
    public boolean isOccupied ()
    {
       return (this.msg.charAt(2) == '1');
    }
    public boolean isDead(){
        if(this.msg.length()>3) {
            return (this.msg.charAt(3) == '1');
        }else{
            return false;
        }
    }
    public String getPosition()
    {
        return msg.substring(2);
    }

    private String tostring (Boolean a){if (a==false){return "0";}else{return "1";}}
    public String toString() {
       return this.msg;
    }

}
