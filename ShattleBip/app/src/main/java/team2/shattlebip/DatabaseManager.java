//Copyright 2018 Liav Albani
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
package team2.shattlebip;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import team2.shattlebip.StatsContract;
public class DatabaseManager {


    // Database Manager Methods & Properties for Managing Database

    public String SQL_QUERY;

    private DBHelper dbHelper;

    public long InsertRecord(String table,int[] values_arr)
    {
        // open database
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // prepare new values according to requested table
        ContentValues values;
        switch (table) {
            case "Stats":
                values = StatsContract.PrepareWriteToTable(values_arr);
                break;
            default:
                return -1;
        }
        if(values == null)
            return -1; //error

        long newRowId = db.insert(table, null, values);
        return newRowId;
    }
    public double winrate(){
        // open database
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(StatsContract.SQL_WINRATE, null);
        Log.d("sql", "rows count is : "+cursor.getCount());
        if(cursor.moveToFirst()){
            double results = cursor.getFloat(cursor.getColumnIndex("Average"));
            cursor.close();
            return results;
        }
        return -1.0;
    }
    public double hitrate(){
        // open database
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor hitcursor = db.rawQuery(StatsContract.SQL_HITSUM, null);
        Cursor misscursor = db.rawQuery(StatsContract.SQL_MISSSUM, null);
        if(hitcursor.moveToFirst()&&misscursor.moveToFirst()){
            double results = (float)hitcursor.getInt(hitcursor.getColumnIndex("Total"))/(hitcursor.getInt(hitcursor.getColumnIndex("Total"))+ misscursor.getInt(misscursor.getColumnIndex("Total")));
            hitcursor.close();
            misscursor.close();
            return results;
        }
        return -1.0;
    }
    public long UpdateRecord(String table,int[] new_values_arr, long id)
    {
        // open database
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // prepare new values according to requested table
        ContentValues values;
        switch (table) {
            case "Stats":
                values = StatsContract.PrepareWriteToTable(new_values_arr);
                break;
            default:
                return -1;
        }
        if(values == null)
            return -1; //error


        // prepare selector
        // Which row to update, based on the title
        String selection = BaseColumns._ID + "=?";
        String[] selectionArgs = { ""+id+"" };

        int idUpdated = db.update(
                table,
                values,
                selection,
                selectionArgs);

        return idUpdated;
    }
    public int DeleteRecord(String table,long id)
    {
        // open database
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // prepare selector
        String selection = BaseColumns._ID + "=?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = {""+id+""};
        // Issue SQL statement.
        int deletedRows = db.delete(table, selection, selectionArgs);
        return deletedRows;
        //return -1;
    }
    public int FindRecord(String table,String column,String value)
    {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                BaseColumns._ID
        };

        // Filter results WHERE "title" = 'My Title'
        String selection = column + " = ?";
        String[] selectionArgs = { value };

        switch (table) {
            case "Stats":
                break;
            default:
                return -1;
        }
        // How you want the results sorted in the resulting Cursor
        String sortOrder = "";
        Cursor cursor = db.query(
                table,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );
        cursor.moveToFirst();
        return cursor.getInt(cursor.getColumnIndex(BaseColumns._ID));

    }
    public Cursor getRecords(String table,String column,String value, String sortOrder)
    {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        // Filter results WHERE "title" = 'My Title'
        String selection = column + " = ?";
        String[] selectionArgs = { value };

        switch (table) {
            case "Stats":
                break;
            default:
                return null;
        }
        // How you want the results sorted in the resulting Cursor
        Cursor cursor = db.query(
                table,   // The table to query
                null,   // The array of columns to return (pass null to get all)
                //projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );
        cursor.moveToFirst();
        return cursor;
    }
    public String getTableAsString(String tableName) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Log.d("sql", "getTableAsString called");
        String tableString = String.format("Table %s:\n", tableName);
        Cursor allRows  = db.rawQuery("SELECT * FROM " + tableName, null);
        if (allRows.moveToFirst() ){
            String[] columnNames = allRows.getColumnNames();
            do {
                for (String name: columnNames) {
                    tableString += String.format("%s: %s\n", name,
                            allRows.getString(allRows.getColumnIndex(name)));
                }
                tableString += "\n";

            } while (allRows.moveToNext());
        }
        allRows.close();
        return tableString;
    }

    public DatabaseManager(Context context) {
        Log.d("sql", "plz create table");
        this.dbHelper = new DBHelper(context);
        //String[] arr = {"Hello world","Hello world","hhh"};
        //long id = InsertRecord("Settings",arr);
        //DeleteRecord("Settings",1);
        //UpdateRecord("Settings",arr,1);
    }


}