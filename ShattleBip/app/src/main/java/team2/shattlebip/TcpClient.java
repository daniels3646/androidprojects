package team2.shattlebip;

        import android.content.Intent;
        import android.util.Log;
        import java.io.BufferedReader;
        import java.io.BufferedWriter;
        import java.io.IOException;
        import java.io.InputStreamReader;
        import java.io.OutputStreamWriter;
        import java.io.PrintWriter;
        import java.net.InetAddress;
        import java.net.Socket;
        import java.net.ServerSocket;

public class TcpClient {

    public static final String TAG = TcpClient.class.getSimpleName();
    public static String SERVER_IP = ""; //server IP address
    public static final int SERVER_PORT = 9090;
    private static Socket socket;
    // message to send to the server
    private String mServerMessage;
    // sends message received notifications
    private OnMessageReceived mMessageListener = null;
    private OnPositionReceived mPositionListener = null;

    // while this is true, the server will continue running
    public static boolean mRun = false;
    // used to send messages
    private PrintWriter mBufferOut;
    // used to read messages from the server
    private BufferedReader mBufferIn;
    public static Message lastMessage = new Message("--");

    /**
     * Constructor of the class. OnMessagedReceived listens for the messages received from server
     */
    //public TcpClient(OnMessageReceived listener) {
    //    mMessageListener = listener;
    //}
    public void setMessageListening(OnMessageReceived listener) {
        mMessageListener = listener;
    }

    public void setAttackListening(OnPositionReceived listener) {
        mPositionListener  = listener;
    }

    public TcpClient() {
    }
    /**
     * Sends the message entered by client to the server
     *
     * @param message text entered by client
     */
    public void sendMessage(final String message) {
        lastMessage = new Message(message);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (mBufferOut != null) {
                    Log.d(TAG, "Sending: " + message);
                    mBufferOut.println(message);
                    mBufferOut.flush();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    /**
     * Close the connection and release the members
     */
    public void stopClient() {

        mRun = false;
        Log.d(TAG, "mrun is false");

        if (mBufferOut != null) {
            mBufferOut.flush();
            mBufferOut.close();
        }

        mMessageListener = null;
        mBufferIn = null;
        mBufferOut = null;
        mServerMessage = null;

        Runtime.getRuntime().addShutdownHook(new Thread(){public void run(){
            try {
                socket.close();
                Log.d(TAG, "closed the socket");
            } catch (IOException e) { /* failed */
                Log.d(TAG, "failed to close socket");
            }
        }});

    }

    public void connect(String servOrClient) {


        try {

            if(servOrClient == "client") {
                //here you must put your computer's IP address.
                InetAddress serverAddr = InetAddress.getByName(SERVER_IP);
                //create a socket to make the connection with the server
                socket = new Socket(serverAddr, SERVER_PORT);
                if(socket.isConnected()){
                    mRun = true;
                    Log.d(TAG, "mrun is true");
                }
            }
            else {
                ServerSocket serverSocket = new ServerSocket(SERVER_PORT);
                socket = serverSocket.accept();
                serverSocket.close();
                mRun = true;
                Log.d(TAG, "mrun is true");
            }
            if(mRun){
                Log.d(TAG, "Connected successfully");
            }

        } catch (Exception e) {
            Log.e("TCP", "C: Error", e);
        }

    }
    public void listen() {

        try {

            //sends the message to the server
            mBufferOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);

            //receives the message which the server sends back
            mBufferIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));


            //in this while the client listens for the messages sent by the server
            while (mRun) {

                Log.d("Game", "ok now im listening");

                mServerMessage = mBufferIn.readLine();
                //mPositionListener != null
                if (mServerMessage != null ) {
                    //response received from server
                    Log.d("received :", mServerMessage);
                    //process server response here....
                    lastMessage = new Message(mServerMessage);
                    if (!lastMessage.isMyTurn())
                    {
                        //Log.d("Game",""+ !lastMessage.isMyTurn());
                        //Log.d("Game",""+ !lastMessage.getGameStage());
                        Log.d("Game", "got something: "+ lastMessage);
                        if ((lastMessage.getGameStage() == Game.Stage.BATTLING))// means that we are attacked
                        {
                            int position = Integer.parseInt(lastMessage.getPosition());
                            Log.d("Game", "the position in which i got attacked is " + lastMessage.getPosition());
                            //call the method messageReceived from MyActivity class
                            mPositionListener.positionReceived(position);
                        }
                    }
                    else{
                        Log.d("Game", "it is my turn and it is the right stage.");
                        return;
                    }

                    //call the method messageReceived from MyActivity class
                    //mMessageListener.messageReceived(mServerMessage);
                }

            }

            Log.d("RESPONSE FROM SERVER", "S: Received Message: '" + mServerMessage + "'");

        } catch (Exception e) {
            Log.e("TCP", "S: Error", e);
        } finally {
            //the socket must be closed. It is not possible to reconnect to this socket
            // after it is closed, which means a new socket instance has to be created.
            Runtime.getRuntime().addShutdownHook(new Thread(){public void run(){
                try {
                    socket.close();
                } catch (IOException e) { /* failed */ }
            }});

        }

    }

    //Declare the interface. The method messageReceived(String message) will must be implemented in the Activity
    //class at on AsyncTask doInBackground
    public interface OnMessageReceived {
        public void messageReceived(String message);
    }

    public interface OnPositionReceived {
        public void positionReceived(Integer position);
    }

}
