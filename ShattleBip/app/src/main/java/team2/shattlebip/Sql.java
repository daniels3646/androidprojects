package team2.shattlebip;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class Sql extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sql);
        TextView winrate = (TextView)findViewById(R.id.winRatio);
        TextView hitrate= (TextView)findViewById(R.id.hitRatio);
        TextView minroundtowin= (TextView)findViewById(R.id.minRoundsToWin);
        winrate.setText("Win Ratio: "+String.valueOf(MainMenu.mDBmanager.winrate()));
        hitrate.setText("Hit Ratio: "+String.valueOf(MainMenu.mDBmanager.hitrate()));
    }
}
