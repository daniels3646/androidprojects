package team2.shattlebip;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.format.Formatter;
import android.view.View;
import android.widget.Button;
import android.app.Activity;
import android.widget.EditText;
import android.os.AsyncTask;
import android.widget.TextView;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import android.net.wifi.WifiInfo;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * @author Paul
 */
public class MainMenu extends Activity implements View.OnClickListener {
    public static TcpClient mTcpClient;
    public static DatabaseManager mDBmanager;
    public static TextView msg;
    public static String ServOrClient = "client";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);
        msg = (TextView) findViewById(R.id.textView);
        mDBmanager = new DatabaseManager(this);
        new NetworkSniffTask().execute(this);
    }

    @Override
    public void onClick(View v) {
        String fullip;
        switch (v.getId()) {
            case R.id.vsbot:
                Game.mode = "bot";
                startActivity(new Intent("team2.shattlebip.MainActivity"));
                break;

            case R.id.credits:
                startActivity(new Intent("team2.shattlebip.Credits"));
                break;
            //case R.id.open:
            //if (mTcpClient != null) {
            //    mTcpClient.sendMessage("this is a message");
            //}
            //break;
            case R.id.connect:
                ServOrClient = "client";
                EditText text1 = (EditText) findViewById(R.id.connectTo);
                String string1 = text1.getText().toString();
                fullip = getDeviceNetworkIp(this);
                Log.e("MainMenu", fullip);
                TcpClient.SERVER_IP = fullip.replaceAll("(.*\\.)\\d+$", "$1") + string1;
                msg.setText("connecting to : " + string1);
                Game.mode = "player";
                new ConnectTask().execute("");
                break;
            case R.id.disconnect:
                if (mTcpClient != null) {
                    mTcpClient.stopClient();
                }
                msg.setText("disconnected");
                break;
            case R.id.listen:
                fullip = getDeviceNetworkIp(this);
                Log.e("MainMenu", fullip);
                msg.setText("listening... your hostid is: " + fullip.substring(fullip.lastIndexOf(".") + 1));
                ServOrClient = "server";
                new ConnectTask().execute("");
                Game.mode = "player";
                //startActivity(new Intent("team2.shattlebip.MainActivity"));
                break;
            case R.id.stats:
                startActivity(new Intent("team2.shattlebip.Sql"));
                break;
        }
    }

    private String getDeviceNetworkIp(Context context) {
        WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo connectionInfo = wm.getConnectionInfo();
        int ipAddress = connectionInfo.getIpAddress();
        if (ipAddress == 0) {
            Log.e("MainMenu", "could not find ip address . this probably means that the devices is a hotspot.");
            return "192.168.43.1";
        }
        String ipaddress = Formatter.formatIpAddress(ipAddress);
        return ipaddress;
    }

    public class ConnectTask extends AsyncTask<String, String, TcpClient> {

        @Override
        protected TcpClient doInBackground(String... message) {
            //we create a TCPClient object
            mTcpClient = new TcpClient();
            mTcpClient.connect(ServOrClient);
            return mTcpClient;
        }

        @Override
        protected void onPostExecute(TcpClient result) {
            Log.d("test", "post Execute");
            if (TcpClient.mRun) {
                startActivity(new Intent("team2.shattlebip.MainActivity"));
            }
        }
    }

    private class NetworkSniffTask extends AsyncTask<Context,String, Void> {

        private static final String TAG = "nstask";
        @Override
        protected Void doInBackground(Context... contexts) {
            Log.d(TAG, "Let's sniff the network");

            try {
                if (contexts[0] != null) {

                    ConnectivityManager cm = (ConnectivityManager) contexts[0].getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    WifiManager wm = (WifiManager) contexts[0].getSystemService(Context.WIFI_SERVICE);

                    WifiInfo connectionInfo = wm.getConnectionInfo();
                    int ipAddress = connectionInfo.getIpAddress();
                    String ipString = Formatter.formatIpAddress(ipAddress);
                    if (ipAddress == 0) {
                        Log.e("MainMenu", "could not find ip address . this probably means that the devices is a hotspot.");
                        ipString ="192.168.43.1";
                    }


                    Log.d(TAG, "activeNetwork: " + String.valueOf(activeNetwork));
                    Log.d(TAG, "ipString: " + String.valueOf(ipString));

                    String prefix = ipString.substring(0, ipString.lastIndexOf(".") + 1);
                    int myhost =Integer.parseInt(ipString.substring(ipString.lastIndexOf(".") + 1));
                    Log.d(TAG, "myhost: " + myhost);
                    Log.d(TAG, "prefix: " + prefix);
                    for (int i = 255; i > 0 ; i--) {
                        //Log.d(TAG, ""+i);
                        String testIp = prefix + String.valueOf(i);

                        InetAddress address = InetAddress.getByName(testIp);
                        boolean reachable = address.isReachable(35);
                        String hostName = address.getCanonicalHostName();
                        if (reachable && i!= myhost) {
                            Log.i(TAG, "Host: " + String.valueOf(hostName) + "(" + String.valueOf(testIp) + ") is reachable!");
                            publishProgress(new String[]{hostName,""+i});
                        }else {
                            if (i == 1 && i!= myhost) {
                                publishProgress(new String[]{"hotspot host", "" + i});
                            }
                        }
                    }
                    Log.d(TAG, "finished echoing");
                }
            } catch (Throwable t) {
                Log.e(TAG, "Well that's not good.", t);
            }

            return null;
        }
        @Override
        protected void onProgressUpdate(String... values) {
            msg.append("\n"+values[1] + " (" + values[0].substring(0, Math.min(values[0].length(),12))+ ") is reachable");
        }

    }
}
