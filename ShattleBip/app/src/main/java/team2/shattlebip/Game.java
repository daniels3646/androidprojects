package team2.shattlebip;

import android.os.AsyncTask;
import android.util.Log;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import java.util.Random;

import static team2.shattlebip.Game.Stage.ARRANGING;
import static team2.shattlebip.Game.Stage.BATTLING;

/**
 * controls game flow
 *
 * @author Vu calls methods defined by Zach, Paul
 *         Zach applies design pattern and replaced game stage strings with enums
 */
public class Game {
    public static String mode = "bot";
    private static Game instance;
    public static Stage stage;
    private Context context;
    private int numCells1side;
    private TextView textViewGameStage, textViewMessage;
    private Button buttonAttack, buttonUpgrade, buttonRestart;
    private GridView gridViewBoard1, gridViewBoard2;
    private AdapterBoard adapterBoard1, adapterBoard2;
    private Player player1, player2;
    private int rounds;
    public static int hits;//public static because the ship class need to access this .
    public static int misses;
    //gameStage (0 for arranging 1 for battling) , whos turn ( 0 for client 1 for server) , position
    private Game() {
    }

    /**
     * Zach's singleton pattern
     */
    public static Game getInstance() {
        if (instance == null)
            instance = new Game();
        return instance;
    }

    /**
     * passes variables from MainActivity
     */
    public void setFields(Context context, int numCells1side,
                          TextView textViewGameStage, TextView textViewMessage,
                          Button buttonAttack, Button buttonUpgrade, Button buttonRestart,
                          GridView gridViewBoard1, GridView gridViewBoard2,
                          AdapterBoard adapterBoard1, AdapterBoard adapterBoard2) {
        this.context = context;
        this.numCells1side = numCells1side;
        this.textViewGameStage = textViewGameStage;
        this.textViewMessage = textViewMessage;
        this.buttonAttack = buttonAttack;
        this.buttonUpgrade = buttonUpgrade;
        this.buttonRestart = buttonRestart;
        this.gridViewBoard1 = gridViewBoard1;
        this.gridViewBoard2 = gridViewBoard2;
        this.adapterBoard1 = adapterBoard1;
        this.adapterBoard2 = adapterBoard2;
    }

    /**
     * [re]starts game by clearing boards and letting bot secretly arrange its fleet
     */
    public void initialize() {
        buttonRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initialize();
            }
        });
        disableClicking();


        adapterBoard1.clear();
        adapterBoard2.clear();
        adapterBoard1.addCells(gridViewBoard1, 1, getNumCellsBoardArea());
        adapterBoard2.addCells(gridViewBoard2, 2, getNumCellsBoardArea());

        player1 = new Player(1);
        player2 = new Player(2);

        //if(Game.mode=="player") {
        //    MainMenu.mTcpClient.sendMessage("hello there im " + MainMenu.ServOrClient);
        //}
        TcpClient.lastMessage = new Message("--");
        //every end of game should add a record with the number of hits , misses and whether you won or lost
        putGameStage(ARRANGING);
        if(Game.mode=="player") {
            Log.d("Game", "my thread is now running");
            new arrange().execute();
        }
        letP1arrange();
    }
    //private void confirmation()//i should put this in a asynctask so the ui wont freeze
    //{// this is a turn switching confirmation
    //    while (!lastMessage.isMyTurn()||(lastMessage.getGameStage()!= this.stage))
    //    {
    //        //Log.d("Game",""+ !lastMessage.isMyTurn());
    //        //Log.d("Game",""+ !lastMessage.getGameStage());
    //        Log.d("Game", "i will wait till he is ready");
    //        while (!lastMessageChanged){
    //            try {
    //                Thread.sleep(333);
    //                Log.d("Game", "waiting.. ");
    //            } catch(InterruptedException e) {
    //                // Process exception
    //            }
    //        }
    //        lastMessageChanged = false;
    //        Log.d("Game", "got something: "+ lastMessage);
    //        if ((lastMessage.getGameStage() == Stage.BATTLING)&&(!lastMessage.isMyTurn()))// means that we are attacked
    //        {
    //            int position = Integer.parseInt(lastMessage.getPosition());
    //            Log.d("Game", "the position in which i got attacked is " + lastMessage.getPosition());
    //            Cell cell = adapterBoard1.getItem(position);
    //            player2.attackCell(cell);
    //            adapterBoard1.notifyDataSetChanged();
    //        }
    //        /*
    //        *here the function should be
    //        *if the game stage is arranging than dont do anything .
    //        *if the game stage is battling than
    //        *   if you are the attacker than this should wait for the whether the attacked unit is a hit or a miss then get out
    //        *   else if you are the attacked than you should be stuck here until you receive a message that says its your turn and just update the board and answer .
    //        */

    //    }
    //    lastMessageChanged = false;
    //    Log.d("Game", "it is my turn and it is the right stage.");
    //}

    private void letP2arrange() {
        MathModel.generateShipPlacement(player2, adapterBoard2, numCells1side); // Zach's code
        Log.d("Game", "yay enabled battling game stage");
        putGameStage(BATTLING);
        attackOrUpgrade();
    }

    private void letP1arrange() {
        gridViewBoard1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cell cell = (Cell) parent.getAdapter().getItem(position);
                player1.addCell(cell);
                adapterBoard1.notifyDataSetChanged();

                if (player1.canAddCell())
                    setMessage(player1.getShips().get(player1.getNumShipsArranged()).getNumCellsToAdd() +
                            " cell(s) left to add to your ship " +
                            (player1.getNumShipsArranged() + 1));
                else {
                    gridViewBoard1.setOnItemClickListener(null);

                    if (checkArrange()){ // Paul's code
                        if (mode == "player") {
                            setMessage("ok you are ready now wait for your opponent to finish");
                            stage = stage.BATTLING;
                            String message = ""+stage.valueOf("BATTLING").ordinal()+p2Turn();
                            MainMenu.mTcpClient.sendMessage(message);
                            Log.d("Game", "ok now i need my thread to finish");


                        }else {
                            putGameStage(BATTLING);
                            letP2arrange();
                        }
                    }
                    else
                        setMessage("Invalid arrangement; click RESTART");
                }
            }
        });
    }

    private boolean checkArrange() {
        ShipArrangement shipArr = new ShipArrangement();
        AdapterBoard adapterBoard = adapterBoard1;
        int c = 0;
        for (int i = 0; i < adapterBoard.getCount(); i++) {
            Cell cell = adapterBoard1.getItem(i);
            if (cell.getStatus() == Cell.Status.OCCUPIED) {
                c = c + 1;
                if (c == 10) {
                    if (((shipArr.checkArrangeLH(adapterBoard)) || (shipArr.checkArrangeLV(adapterBoard)))) {
                        if (((shipArr.checkArrangeMH(adapterBoard)) || (shipArr.checkArrangeMV(adapterBoard)))) {
                            if (((shipArr.checkArrangeSH(adapterBoard)) || (shipArr.checkArrangeSV(adapterBoard)))) {
                                return true;
                            }
                        }
                    }

                }
            }
        }
        return false;
    }

    private void attackOrUpgrade() {

        buttonAttack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonUpgrade.setOnClickListener(null);
                buttonAttack.setOnClickListener(null);
                if (mode == "player") {
                    if(!TcpClient.lastMessage.isMyTurn()) {
                        setMessage("its your opponents turn");
                    }
                    else{
                        letP1attack();
                    }
                }
                else {
                    letP1attack();
                }
            }
        });

        buttonUpgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player1.upgrade();
                buttonUpgrade.setOnClickListener(null);
                if (mode == "player") {
                    if(!TcpClient.lastMessage.isMyTurn()) {
                        setMessage("its your opponents turn");
                    }
                    else{
                        String message = ""+stage.valueOf("BATTLING").ordinal()+p2Turn()+'1';
                        MainMenu.mTcpClient.sendMessage(message);
                        Log.d("Game", "upgraded , sent opponent turn switch message");
                        setMessage("its your opponents turn");
                        letP2attack();
                    }
                }
                else {
                    letP2attack();
                }
            }
        });

    }

    private char p1Turn()
    {
        if (MainMenu.ServOrClient == "server"){
            return '1';
        }
        else{
            return '0';
        }
    }

    private char p2Turn()
    {
        if (MainMenu.ServOrClient == "server"){
            return '0';
        }
        else{
            return '1';
        }
    }

    private void letP1attack() {
        gridViewBoard2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cell cell = (Cell) parent.getAdapter().getItem(position);
                if (mode == "player") {
                    cell.position = position;
                    new attack().execute(cell);
                }
                else {
                    player1.attackCell(cell);
                    adapterBoard2.notifyDataSetChanged();

                    if (!player2.isAlive()) {
                        disableClicking();
                        setMessage("you won; click RESTART");
                    } else if (player1.canAttack()) {
                        Ship ship = player1.getNextShipCanAttack();
                        setMessage(ship.getNumAttacksLeft() + " attack(s) left for your ship " +
                                (player1.getShips().indexOf(ship) + 1));
                    } else {
                        gridViewBoard2.setOnItemClickListener(null);
                        player1.resetNumsAttacksMade();
                        letP2attack();
                    }
                }
            }
        });
    }

    private void letP2attack() {
        if (mode == "player") {

            new attacked().execute();

        }else{
            Random random = new Random();
            while (player2.canAttack()) {
                Cell cell;
                do {
                    cell = adapterBoard1.getItem(random.nextInt(getNumCellsBoardArea()));
                }
                while (cell.getStatus() == Cell.Status.HIT ||
                        cell.getStatus() == Cell.Status.MISSED);
                player2.attackCell(cell);
                adapterBoard1.notifyDataSetChanged();

                if (!player1.isAlive()) {
                    disableClicking();
                    setMessage("you lost; click RESTART");
                    break;
                }
            }
            if (player1.isAlive()) {
                player2.resetNumsAttacksMade();
                attackOrUpgrade();
            }

        }
    }

    private void putGameStage(Stage stage) {
        this.stage = stage;
        String msg = "Game stage: " + stage;
        textViewGameStage.setText(msg);
        describeGameStage();
    }

    private void describeGameStage() {
        String msg;
        if (stage == ARRANGING)
            msg = "tap cell on your board to arrange your " +
                    player1.getNumShips() + " ships";
        else if (stage == BATTLING)
            msg = "click ATTACK or UPGRADE";
        else
            msg = "tap cell on bot's board to attack its " +
                    player2.getNumShipsAlive() + " alive ship(s)";
        setMessage(msg);
    }

    private void setMessage(String msg) {
        textViewMessage.setText("Message: " + msg);
    }

    private void disableClicking() {
        buttonAttack.setOnClickListener(null);
        buttonUpgrade.setOnClickListener(null);
        gridViewBoard1.setOnItemClickListener(null);
        gridViewBoard2.setOnItemClickListener(null);
    }

    private int getNumCellsBoardArea() {
        return (int) Math.pow(numCells1side, 2);
    }

    //    Vu's naive approach to weakly randomizing bot's fleet arrangement
    //    Now use Zach's approach instead
    private void generateShipPlacementDeprecated() {
        Random random = new Random();
        for (int i = 0; i < player2.getNumShips(); i++) {
            Ship ship = player2.getShips().get(i);
            int randomRow = i * 2 + random.nextInt(2),
                    randomColumn = random.nextInt(2),
                    randomPosition = randomRow * numCells1side + randomColumn;
            for (int j = 0; j < ship.getNumCells(); j++) {
                Cell cell = adapterBoard2.getItem(randomPosition + j);
                cell.setStatus(Cell.Status.OCCUPIED);
                ship.addCell(cell);
            }
        }
    }

    public enum Stage {
        ARRANGING, BATTLING
    }
    public class arrange extends AsyncTask<Void , Integer, Void > {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("Game", "pre execute of arrange");

        }

        @Override
        protected Void doInBackground(Void... something) {

            MainMenu.mTcpClient.listen();
            //should just loop here waiting for the stage to become BATTLING by the UI thread
            while (stage!=Stage.BATTLING){
                try {
                    Thread.sleep(333);
                    Log.d("Game", "got confirmation ... waiting till i finish my arrangement");
                } catch(InterruptedException e) {
                    // Process exception
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            Log.d("Game", "post Execute arranging");
            putGameStage(BATTLING);
            Log.d("Game", "the game stage is : "+stage);
            Log.d("Game", " ismyturn? : "+TcpClient.lastMessage.isMyTurn());
            MathModel.generateShipPlacement(player2, adapterBoard2, numCells1side); // just so P2 have ship instances and the compiler wont scream at me
            if (!TcpClient.lastMessage.isMyTurn()) {
                setMessage("its your opponents turn");
                letP2attack();
            }
            else {
                setMessage("its your turn");
            }
            attackOrUpgrade();


        }
    }
    public class attack extends AsyncTask<Cell, Integer, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("Game", "pre execute of attack");

        }

        @Override
        protected Boolean doInBackground(Cell... cell) {
            Log.d("Game", "the position of the button i clicked is: "+cell[0].position);

            //we create a TCPClient object
            MainMenu.mTcpClient.setAttackListening(new TcpClient.OnPositionReceived() {
                @Override
                //here the messageReceived method is implemented
                public void positionReceived(Integer position) {
                    //this method calls the onProgressUpdate
                    publishProgress(position);
                }
            });
            String message = ""+stage.valueOf("BATTLING").ordinal()+p1Turn()+cell[0].position;
            MainMenu.mTcpClient.sendMessage(message);
            MainMenu.mTcpClient.listen();
            //waiting for reply
            player1.attackCell(TcpClient.lastMessage.isOccupied(),cell[0]); // i need to put this in the thread after we get answer

            if (TcpClient.lastMessage.isDead()) {//if !player2.isAlive()
                disableClicking();
                return null;
            } else if (player1.canAttack()) {
                return false;
            } else {
                player1.resetNumsAttacksMade();
                return true;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {//is the attack finished
            adapterBoard2.notifyDataSetChanged();
            if(result == null) {
                setMessage("you won; click RESTART");
                MainMenu.mDBmanager.InsertRecord("Stats", new int[]{hits,misses,1});
                Log.d("Game",MainMenu.mDBmanager.getTableAsString("Stats"));
            }
            else{
                if (result) {
                    //here i should call letP2attack()
                    //send message informing that we stopped attacking
                    gridViewBoard2.setOnItemClickListener(null);
                    String message = ""+stage.valueOf("BATTLING").ordinal()+p2Turn();
                    MainMenu.mTcpClient.sendMessage(message);
                    setMessage("its your opponents turn");
                    letP2attack();
                }
                else{
                    Ship ship = player1.getNextShipCanAttack();
                    setMessage(ship.getNumAttacksLeft() + " attack(s) left for your ship " +
                            (player1.getShips().indexOf(ship) + 1));
                }
            }
        }
    }

    public class attacked extends AsyncTask<Void , Integer, Boolean > {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("Game", "pre execute of attacked");

        }

        @Override
        protected Boolean doInBackground(Void... something) {

            //we create a TCPClient object
            MainMenu.mTcpClient.setAttackListening(new TcpClient.OnPositionReceived() {
                @Override
                //here the messageReceived method is implemented
                public void positionReceived(Integer position) {
                    //this method calls the onProgressUpdate
                    publishProgress(position);
                }
            });
            MainMenu.mTcpClient.listen();
            if(TcpClient.lastMessage.didHeUpgrade()){
                return true;
            }
            return false;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            Cell cell = adapterBoard1.getItem(values[0]);
            player2.attackCell(cell);
            adapterBoard1.notifyDataSetChanged();
            // here i should message back whether the attacker hit or not
            String message ;
            if (cell.getStatus() == Cell.Status.HIT){
               if(!player1.isAlive()) {
                   message = ""+stage.valueOf("BATTLING").ordinal()+p2Turn()+'1'+'1';
                   setMessage("you lost; click RESTART");
                   MainMenu.mDBmanager.InsertRecord("Stats", new int[]{hits,misses,0});
                   Log.d("Game",MainMenu.mDBmanager.getTableAsString("Stats"));


               }else{
                message = ""+stage.valueOf("BATTLING").ordinal()+p2Turn()+'1';
               }
            }
            else{
                message = ""+stage.valueOf("BATTLING").ordinal()+p2Turn()+'0';
            }
            MainMenu.mTcpClient.sendMessage(message);
        }

        @Override
        protected void onPostExecute(Boolean didHeUpgrade) {
            if (didHeUpgrade) {
                setMessage("he upgraded. your turn");
                player2.upgrade();
            }else{
                setMessage("its your turn");
            }
            Log.d("Game", "post Execute of attacked");
            //here decide if you want to go to letP1Attack
            player2.resetNumsAttacksMade();
            attackOrUpgrade();
        }
    }
}
